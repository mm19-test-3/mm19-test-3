/*
** testClient for mm19!! In C!
**
** The purpose of this class is to do the boring jsonifying for our AI. Also, there is a function to free things here.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

//holds ship info returned from server
typedef struct{
	char type;
	int x;
	int y;
	int health;
	int ID;
	char orient;
} ship;

//holds data on hit reports from server
typedef struct{
	int x;
	int y;
	int success;
} hit;

//holds data from the server's ping reports
typedef struct{
	int ID;
	int dist;
} ping;

//holds data from the server's action response
typedef struct{
	int ID;
	char result;
} actionResponse;

//This structure is for defining your actions!
typedef struct{
	int ID;
	char * actID;
	int x;
	int y;
	int extra;
} action;


//returns JSON for declaring a ship
char * makeShip(char type, int x, int y, char orient){
	char * buf = malloc(70);
	char * temp=buf;
	if (type=='M'){
		sprintf(temp, "\"mainShip\" : { "); //print to string
		temp+=strlen(temp);
	} else {
		sprintf(temp, "{ \"type\" : \"%c\", ", type);
		temp+=strlen(temp);
	}
	sprintf(temp, "\"xCoord\" : %d, \"yCoord\" : %d, \"orientation\" : \"%c\" }", x, y, orient);
	
	return buf;
}

//returns JSON for an action
char * writeAction(action a){
	char * str = malloc(128); //PROBABLY enough 
	sprintf(str, "{\"ID\":%d, \"actionX\":%d, \"actionY\":%d, \"actionExtra\":%d, \"actionID\":\"%s\"}", a.ID, a.x, a.y, a.extra, a.actID);
	return str;
}


//given list of actions ending in action with ID < 0
int writeJSON(char * buf, char * token, action * actions){
	char * temp=buf;
	sprintf(temp, "{ \"playerToken\" : \"%s\", \"shipActions\" : [ ", token);
	temp=temp+strlen(temp);
	int i;
	if(actions!=NULL) for(i=0; actions[i].ID>-1; i++){
		if(i>0) {
			sprintf(temp, ", ");
			temp+=strlen(temp);
		}
		char * str=writeAction(actions[i]);
		sprintf(temp, "%s", str);
		temp=temp+strlen(temp);
		free(str);
	}
	sprintf(temp, " ] }\n");
}

//frees some stuff
int freeAll(ship * ships, char ** errors, hit * hits, actionResponse * responses, ping * pings){
	if(ships!=NULL) free(ships);
	if(hits!=NULL) free(hits);
	int i=0;
	if(errors!=NULL) for(i=0; errors[i]!=0; i++){
		free(errors[i]);
	}
	if(errors!=NULL) free(errors);
	if(responses!=NULL) free(responses);
	if(pings!=NULL) free(pings);
}
