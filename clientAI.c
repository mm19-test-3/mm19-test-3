/*
** testClient for mm19!! In C!
**
** This is the part where you write your AI.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include "AIhelper.c"

#define MAXDATASIZE 8192 //This is just a large number, hopefully large enough for this competition. If it is not, increase this value.



/*
Initialize the ships and return a buffer. Basically, just decide what you want your ships to be.

name is taken from your name.txt file.

If you're going to change the number of ships (not sure why you'd do that), you need to fix the string as well
*/
char * init(char * name){
	char * buf= malloc(MAXDATASIZE);
	char * ship1 = makeShip('M', 5, 0, 'H'); //ship1 must always be main ship!!
	char * ship2 = makeShip('P', 6, 6, 'H');
	char * ship3 = makeShip('P', 8, 8, 'H');
	char * ship4 = makeShip('P', 10, 10, 'H');
	char * ship5 = makeShip('P', 12, 12, 'H');
	char * ship6 = makeShip('P', 14, 14, 'H');
	char * ship7 = makeShip('P', 16, 16, 'H');
	char * ship8 = makeShip('P', 18, 17, 'H');
	char * ship9 = makeShip('P', 20, 18, 'H');
	char * ship10 = makeShip('P', 22, 20, 'H');
	char * ship11 = makeShip('P', 24, 30, 'H');
	char * ship12 = makeShip('D', 26, 34, 'H');
	char * ship13 = makeShip('D', 28, 56, 'V');
	char * ship14 = makeShip('D', 30, 78, 'H');
	char * ship15 = makeShip('D', 32, 4, 'H');
	char * ship16 = makeShip('D', 34, 34, 'H');
	char * ship17 = makeShip('D', 36, 36, 'H');
	char * ship18 = makeShip('D', 38, 38, 'H');
	char * ship19 = makeShip('D', 40, 40, 'H');
	sprintf(buf, "{ \"playerName\" : \"%s\", %s, \"ships\" : [ %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ] }\n", name, ship1, ship2, ship3, ship4, ship5, ship6, ship7, ship8, ship9, ship10, ship11, ship12, ship13, ship14, ship15, ship16, ship17, ship18, ship19);
	free(ship1);
	free(ship2);
	free(ship3);
	free(ship4);
	free(ship5);
	free(ship6);
	free(ship7);
	free(ship8);
	free(ship9);
	free(ship10);
	free(ship11);
	free(ship12);
	free(ship13);
	free(ship14);
	free(ship15);
	free(ship16);
	free(ship17);
	free(ship18);
	free(ship19);
	return buf;
}




/*
This function passes you all the relevent data (response, resources, ships, errors, actionresponses, and pings)

buf is used to write your server response into. Don't touch it, just pass it to WriteJSON.

Don't free anything that isn't already being freed! I'm freeing it later on.

Returns 0 to continue (if the server wasn't requesting an action), 1 to quit, 2 to send

The ship, responses, and pings arrays all end with an element where ID < 0 
(the last element in the array always has ID < 0, so stop iterating through it!)

The errors list ends with a null string

the hits list ends with a success value < 0

*/
int getTurn(char * buf,  char * token, int response, int resources, ship * ships, char ** errors, hit * hits, actionResponse * responses, ping * pings){
	if(response==-1 || response==9001) {
		freeAll(ships, errors, hits, responses, pings);
		return 1;
	}
	if(response!=100) {
		freeAll(ships, errors, hits, responses, pings);
		return 0;
	}

	//Find main ship, tell it to fire at its own location!

	//The actions list MUST end with an action of ID = -1 !!
	action * actions=malloc(sizeof(action)*2);
	actions[1].ID=-1;
	actions[0].actID=malloc(2);
	int i=0;
	if(ships!=NULL) for(i=0; ships[i].ID>-1; i++){
		if(ships[i].type=='M') {
			actions[0].ID=ships[i].ID;
			actions[0].x=ships[i].x;
			actions[0].y=ships[i].y;
			actions[0].extra=0;
			sprintf(actions[0].actID, "F"); //The length of this string should not exceed 2!
			break;
		}
	}
	writeJSON(buf, token, actions);
	free(actions[0].actID);
	free(actions);
	freeAll(ships, errors, hits, responses, pings);
	return 2;
}